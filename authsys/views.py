# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, redirect
from django.views.decorators.csrf import csrf_protect
from django.template.context_processors import csrf
from django.utils.decorators import method_decorator
from django.contrib import auth


class AuthSystem(object):
    def __init__(self):
        super(AuthSystem, self).__init__()

    @staticmethod
    def initial_data(request, page_title=''):
        data = {}
        data.update(csrf(request))
        data['page_title'] = page_title

        return data

    @method_decorator(csrf_protect)
    def login(self, request):
        data = self.initial_data(request, 'Войти')

        if request.POST:
            username = request.POST.get('username', '')
            password = request.POST.get('password', '')
            user = auth.authenticate(username=username, password=password)
            if user is not None:
                auth.login(request, user)
                return redirect('/')
            else:
                data['login_error'] = 'Неправильный логин и/или пароль.'
                return render_to_response("login.html", data)
        else:
            return render_to_response('login.html', data)

    def logout(self, request):
        auth.logout(request)
        return redirect('/')
