from django.conf.urls import url

from .views import AuthSystem


authsys = AuthSystem()

urlpatterns = [
    url(r'^login', authsys.login, name='login'),
    url(r'^logout', authsys.logout, name='logout'),

]
