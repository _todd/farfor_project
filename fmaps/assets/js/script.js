$(document).ready(function($) {

    // Автокомплит
    var $cityBox = $('#city');
    getCities(function(cities) {
        $cityBox.autocomplete({
            source:function(request, response) {
                response(cities.filter(function(el) {
                    return el.toLowerCase().indexOf(request.term.toLowerCase()) > -1;
                }));
            }
         });
    });

    // ----------

    // Получить список всех городов
    function getCities(handler) {
        $.ajax({
            type: 'POST',
            url: 'ajax/get_cities/',
            data: {},
            dataType: 'json',
            cache: false,
            success: function(data) {
                if (data['result'] == 'ok') {
                    handler(data['cities']);
                }
            }
        });
    };

    // Получить список точек в зависимости от выбранного города
    function getPoints(handler) {
        $.ajax({
            type: 'POST',
            url: 'ajax/get_points/',
            data: {'city': $('#city').val() },
            dataType: 'json',
            cache: false,
            success: function(data) {
                console.log(data['result']);
                $('#point').empty();
                $('#point').prepend('<option value="">---</option>');
                $.each(data['points'], function(i, item) {
                    $('#point').append('<option value="' + item[0] + '">' + item[1] + '</option>');
                });
                $('#point').change();
            }
        })
    }

    // Получить количество маршрутов, которые будут загружены
    function getRoutesCount(handler) {
        $.ajax({
            type: 'POST',
            url: 'ajax/get_routes_count/',
            data: $('#form-map').serialize(),
            dataType: 'json',
            cache: false,
            success: function(data) {
                if (data['routesCount'] > 0)
                    $('#msg-label').html("Будет загружено рейсов: " + data['routesCount']);
                else
                    $('#msg-label').html('На данный период рейсов нет');
            }
        })
    }

    // map
    // Добавление маршруты на карту
    function addRoutesToMap(ymap, routes) {
        console.log('Добавляем маршруты');

        routes.forEach(function(route) {
            try {
                ymap.geoObjects.add(route);
            } catch (err) {
                conlose.log(err);
            }
        });

        console.log('Маршруты добавлены');
    }

    var routesLoaded = 1,
        routesCount = 0;
    // по событию загрузки маршрута добавить его протяженность в таблицу
    function addDistanceToTable(route, i) {
        multiRoute.model.events.add("requestsuccess", function(event) {
            var routes = event.get("target").getRoutes();
            $('#distanceColumn' + i).html(routes[0].properties.get('distance').text);
            routesLoaded++;

           if (routesLoaded == routesCount) {
                $('#msg-label').html("Маршрутов загружено: " + routesCount);
                $('#map').css({'display': 'block'});
                $('#upd-btn').removeClass('disabled');
           } else {
               $('#msg-label').html('Обработано ' + routesLoaded + ' из ' + routesCount);
           }
       });

    }

    // Установить карту
    function setMap(center, routes) {

        console.log('Создаем карту.');
        routesCount = routes.length;

        if (routesCount == 0) {
            $('#msg-label').text('На данный период рейсов нет');
            $('#upd-btn').removeClass('disabled');
            return;
        };

        $('#msg-label').text(routesCount + ' (Идет загрузка...)');

        $('.map-container').html('<div id="map"></div>');
        $('#map').css({'display': 'none'});

        ymaps.ready(function() {
            ymap = new ymaps.Map('map', {
                center: center,
                zoom: 10,
                controls: ['smallMapDefaultSet']
            }, {
                // ...
            });

            addRoutesToMap(ymap, routes);

        });

        console.log('Карта создана.');
    }

    var colors = ['#ff0000', '#00ff00', '#0000ff', '#3947d0', '#53d621', '#5137bc']

    // Подготовить маршруты для помещения на карту
    function prepareRoutes(rowRoutes) {
        console.log(rowRoutes);

        var i = 1;
        var colorIndex = 0;
        var multiRoutes = [];
        for (var src in rowRoutes) {
            for (var dst in rowRoutes[src]) {
                multiRoute = new ymaps.multiRouter.MultiRoute({
                    referencePoints: [
                       src, (rowRoutes[src][dst][1][0] != null ? rowRoutes[src][dst][1] : rowRoutes[src][dst][0])
                    ],
                    params: {
                       results: 1
                    }
                }, {
                    // boundsAutoApply: true,
                    wayPointStartIconLayout: (dst == 0 ? 'islands#stretchyIcon' : 'none'),
                    wayPointStartIconFillColor: '#b3b3b3',
                    wayPointStartIconColor: colors[colorIndex % colors.length], // '#e50000',
                    wayPointStartIconContentLayout:
                        (dst == 0 ? ymaps.templateLayoutFactory.createClass(src.slice(8, src.length)) : ''),

                    wayPointFinishIconLayout: 'none',

                    routeStrokeWidth: 3,
                    routeActiveStrokeColor: colors[colorIndex % colors.length],
                });

                multiRoutes.push(multiRoute);

                addDistanceToTable(multiRoute, i);

                // Добавляем данные в таблицу
                $('#flights-table > tbody:last-child').append(
                    '<tr>' +
                        '<td class="text-right">' + i + '</td>' +
                        '<td class="text-center">' + src + '</td>' +
                        '<td class="text-center">' + rowRoutes[src][dst][0] + '</td>' +
                        '<td class="text-center" id="distanceColumn' + i + '"></td>' +
                    '</tr>'
                );

                i++;
            }

            colorIndex++;
        };

        return multiRoutes;
    }

    function updateMap() {
        routesLoaded = 0;

        $('.flights-table').css({'display': 'block'});
        $('#flights-table').find('tr:gt(0)').remove();

        console.log('Загружаем данные...');

        $.ajax({
            type: 'POST',
            url: 'ajax/get_routes/',
            data: $('#form-map').serialize(),
            dataType: 'json',
            cache: false,
            success: function(data) {
                if (data['result'] == 'ok') {
                    setMap(data['center'], prepareRoutes(data['routes']));
                } else {
                    $('#msg-label').text(data['errormsg']);
                    $('#upd-btn').removeClass('disabled');
                }
            },
            error: function() {
                $('#msg-label').text('Ошибка. Попробуйте повторить запрос');
            }
        });

        console.log('Данные загружены.');
    }

    // --------

    //
    $('#city').on('change', function() {
        $('#msg-label').empty();
        getPoints();
    });

    $('.form-filter').on('change', function() {
        $('#msg-label').empty();
        getRoutesCount();
    });

    $('#upd-btn').on('click', function(e) {
        e.preventDefault();
        if (!($('#upd-btn').hasClass('disabled'))) {
            $('#upd-btn').addClass('disabled');     // на время загрузки отключить кнопку
            updateMap();
        }
    })



})