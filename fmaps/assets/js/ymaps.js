//ymaps.ready(init);
//
//function init() {
//  // создаем карту
//  var myMap = new ymaps.Map('map', {
//    center: [54.74, 55.97],
//    zoom: 10,
//    controls: ['smallMapDefaultSet']
//  }, {
//
//  });
//
//
//  // массив маршрутов
//  var multiRoutes = [
//  // с 8 марта, 8
//  // до Бабушкина, 17г
//  new ymaps.multiRouter.MultiRoute({
//    referencePoints: [
//    'Уфа, 8 марта, 8',
//    'Уфа, Бабушкина, 17г'
//    ],
//    params: {
//      results: 1
//    }
//  }, {
//    boundsAutoApply: true,
//
//  // внешний вид точки:
//  // wayPointStartIconColor: '#333',
//  // wayPointStartIconFillColor: '#b3b3b3',
//
//  // внешний вид для последней путевой точки
//  // wayPointFinishIconLayout: 'default#image',
//  }),
//
//  // с 8 марта 8
//  // до Карла Маркса, 12
//  new ymaps.multiRouter.MultiRoute({
//    referencePoints: [
//    'Уфа, 8 марта, 8',
//    'Уфа, Карла Маркса, 12'
//    ],
//    params: {
//      results: 1
//    }
//  }, {
//    boundsAutoApply: true,
//    wayPointStartIconLayout: 'none',
//
//
//  }),
//
//  // с 8 марта 8
//  // до Карла Маркса, 12
//  new ymaps.multiRouter.MultiRoute({
//    referencePoints: [
//    'Уфа, 8 марта, 8',
//    'Уфа, Рихарда Зорге, 7'
//    ],
//    params: {
//      results: 1
//    }
//  }, {
//    boundsAutoApply: true,
//    wayPointStartIconLayout: 'none',
//  }),
//  ];
//
//  multiRoutes.forEach(function(multiRoute) {
//    myMap.geoObjects.add(multiRoute);
//  });
//};