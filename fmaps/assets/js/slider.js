$("#slider-range").slider({
    range: true,
    min: 0,
    max: 1440,
    step: 15,
    values: [540, 1020],
    slide: function (e, ui) {

        // timeFrom
        var hours1 = Math.floor(ui.values[0] / 60);
        var minutes1 = ui.values[0] - (hours1 * 60);

        if (hours1 < 10) hours1 = '0' + hours1;
        if (minutes1 < 10) minutes1 = '0' + minutes1;
        if (hours1 == 24) { hours1 = '23'; minutes2 = '45'; }
        $('.slider-time1').val(hours1 + ':' + minutes1).change();

        // timeTo
        var hours2 = Math.floor(ui.values[1] / 60);
        var minutes2 = ui.values[1] - (hours2 * 60);

        if (hours2 < 10) hours2 = '0' + hours2;
        if (minutes2 < 10) minutes2 = '0' + minutes2;
        if (hours2 == 24) { hours2 = '23'; minutes2 = '45'; }
        $('.slider-time2').val(hours2 + ':' + minutes2).change();


    }
});