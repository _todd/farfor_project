from django.conf.urls import url

from .views import FMaps


fmaps = FMaps()

urlpatterns = [
    url(r'^ajax/get_cities/$', fmaps.get_cities, name='get_cities'),
    url(r'^ajax/get_routes/$', fmaps.get_routes, name='get_routes'),
    url(r'^ajax/get_points/$', fmaps.get_points, name='get_points'),
    url(r'^ajax/get_routes_count/$', fmaps.get_routes_count, name='get_routes_count'),

    url(r'^charts/$', fmaps.index_charts, name='charts'),
    url(r'^$', fmaps.index_map, name='index'),
]
