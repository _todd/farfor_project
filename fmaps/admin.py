# coding: utf-8

from django.contrib import admin

from .models import *


class CityAdmin(admin.ModelAdmin):
    """
        Отображение модели Города
    """
    list_display = ('c_name', 'lon', 'lat')
    search_fields = ('c_name', )


class GeoStreetAdmin(admin.ModelAdmin):
    """
        Отображение модели Улицы
    """
    list_display = ('id', 'related_city', 'street_name')
    list_filter = ('city__c_name', )

    def related_city(self, obj):
        return obj.city.c_name
    related_city.short_description = 'Город'


class ClientAdmin(admin.ModelAdmin):
    """
        Отображение модели Клиента
    """
    list_display = ('id', 'city', 'street', 'house_num', 'lon', 'lat')
    list_filter = ('city__c_name', )


class OrderAdmin(admin.ModelAdmin):
    """
        Отображение модели Заказа
    """
    list_display = ('id', 'related_city', 'point', 'address', 'persons', 'delivery_period')
    list_filter = ('city__c_name', )

    def related_city(self, obj):
        return obj.city.c_name
    related_city.short_description = 'Город'


class FlightAdmin(admin.ModelAdmin):
    """
        Отображение модели Рейса
    """
    list_display = ('id', 'created_at', 'point')
    list_filter = ('point__city__c_name', )


class PointAdmin(admin.ModelAdmin):
    """
        Отображение модели Точки
    """
    list_display = ('id', 'related_city', 'address', 'lon', 'lat')
    list_filter = ('city__c_name', )

    def related_city(self, obj):
        return obj.city.c_name
    related_city.short_description = 'Город'


class FlightOrderAdmin(admin.ModelAdmin):
    """
        Отображение модели Рейса-Заказа
    """
    list_display = ('id', 'related_flight', 'related_order')

    def related_flight(self, obj):
        return '{}'.format(obj.flight.point)
    related_flight.short_description = 'Откуда'

    def related_order(self, obj):
        return '{}'.format(obj.order.address)
    related_order.short_description = 'Куда'


admin.site.register(GeoStreet, GeoStreetAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(Point, PointAdmin)
admin.site.register(Flight, FlightAdmin)
admin.site.register(FlightOrder, FlightOrderAdmin)
