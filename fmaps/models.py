# -*- coding: utf-8 -*-

from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class City(models.Model):
    """
        Город
    """
    class Meta:
        db_table = 'city'
        verbose_name_plural = 'cities'

    c_name = models.CharField(max_length=50, verbose_name="Город")
    lon = models.FloatField(verbose_name='Долгота')
    lat = models.FloatField(verbose_name='Широта')

    def __str__(self):
        return self.c_name


@python_2_unicode_compatible
class GeoStreet(models.Model):
    """
        Улица с привязкой к городу
    """
    class Meta:
        db_table = 'geo_street'
        verbose_name_plural = 'streets'

    city = models.ForeignKey(City, db_column='city_id', verbose_name='Город')
    street_name = models.CharField(max_length=50, verbose_name='Название улицы')

    def __str__(self):
        return '{}, {}, {}'.format(self.id, self.city.c_name, self.street_name)


@python_2_unicode_compatible
class Point(models.Model):
    """
        Точка
    """
    class Meta:
        db_table = 'point'

    city = models.ForeignKey(City, db_column='city_id', verbose_name='Город')
    point_name = models.CharField(max_length=50, verbose_name='Название точки')
    address = models.CharField(max_length=255, verbose_name='Адрес')
    lat = models.FloatField(verbose_name='Широта')
    lon = models.FloatField(verbose_name='Долгота')
    cooking_time = models.IntegerField(verbose_name='Время готовки, мин')

    def __str__(self):
        return '{}'.format(self.address)


@python_2_unicode_compatible
class Client(models.Model):
    """
        Клиент
    """
    class Meta:
        db_table = 'client'

    client_id = models.IntegerField()
    city = models.ForeignKey(City, db_column='city_id', verbose_name='Город')
    street = models.ForeignKey(GeoStreet, db_column='street_id', verbose_name='Улица')
    house_num = models.CharField(max_length=10, verbose_name='Номер дома')
    lat = models.FloatField(verbose_name='Широта')
    lon = models.FloatField(verbose_name='Долгота')

    def __str__(self):
        return '{}, {}, {}'.format(self.city.c_name, self.street.street_name, self.house_num)


@python_2_unicode_compatible
class Order(models.Model):
    """
        Заказ
    """
    class Meta:
        db_table = 'orders_order'

    city = models.ForeignKey(City, db_column='city_id', verbose_name='Город')
    point = models.ForeignKey(Point, db_column='point_id', verbose_name='Адрес отправления')
    client = models.IntegerField(db_column='client_id', verbose_name='Заказчик')
    address = models.ForeignKey(Client, db_column='address_id', verbose_name='Адрес доставки')
    persons = models.IntegerField(db_column='persons', verbose_name='Кол-во человек')
    delivery_period = models.IntegerField(db_column='delivery_period', verbose_name='Время доставки, мин')

    def __str__(self):
        return '{}'.format(self.id)


@python_2_unicode_compatible
class Flight(models.Model):
    """
        Рейс
    """
    class Meta:
        db_table = 'orders_flight'

    created_at = models.DateTimeField(verbose_name='Дата создания')
    point = models.ForeignKey(Point, db_column='point_id', verbose_name='Точка')
    distance = models.IntegerField(verbose_name='Расстояние')

    def __str__(self):
        return '{}'.format(self.id)


@python_2_unicode_compatible
class FlightOrder(models.Model):
    """
        Промежуточная таблица "Рейс - Заказ"
    """
    class Meta:
        db_table = 'flight_order'
        verbose_name_plural = 'Flights - Orders'

    flight = models.ForeignKey(Flight, db_column='flight_id', verbose_name='Рейс')
    order = models.ForeignKey(Order, db_column='order_id', verbose_name='Заказ')

    def __str__(self):
        return '{}, {}'.format(self.flight.id, self.order.id)
