# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, redirect
from django.views.decorators.csrf import csrf_protect
from django.template.context_processors import csrf
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib import auth

from datetime import datetime

from .models import City, Point
from .sqlmanager import SqlManager


class FMaps(object):
    def __init__(self):
        super(FMaps, self).__init__()
        self.default_page_title = 'Главная'
        self.sqlmanager = SqlManager()

    # Формируем начальные данные для ответа
    @staticmethod
    def initial_data(request, page_title=''):
        data = {}
        data.update(csrf(request))
        data['page_title'] = page_title

        return data

    # Главная
    @method_decorator(login_required)
    def index_map(self, request):
        data = self.initial_data(request, page_title='Главная')

        data['cities'] = City.objects.all()
        data['username'] = auth.get_user(request).username

        now = datetime.now()
        data['date'] = '{}-{:>02}-{:>02}'.format(now.year, now.month, now.day)

        return render_to_response('index.html', data)

    # Графики
    def index_charts(self, request):
        data = self.initial_data(request, page_title='Графики')

        return render_to_response('charts.html', data)

    """
        ajax's
    """
    @staticmethod
    def get_cities(request):
        cities = City.objects.all()
        to_json = {'cities': [city.c_name for city in cities], 'result': 'ok'}
        return JsonResponse(to_json)

    @staticmethod
    def get_points(request):
        city = request.POST.get('city', '')
        points = Point.objects.filter(city__c_name=city)
        to_json = {'points': [(point.id, point.point_name) for point in points], 'result': 'ok'}
        return JsonResponse(to_json)

    @method_decorator(csrf_protect)
    def get_routes_count(self, request):

        if request.is_ajax and request.POST:
            try:
                fcity = request.POST.get('city')
                fdate = request.POST.get('date')
                fpoint = request.POST.get('point')
                ftime_from = request.POST.get('timeFrom')
                ftime_to = request.POST.get('timeTo')

                if ftime_from > ftime_to:
                    return JsonResponse({'result': 'fail',
                                         'errormsg': 'Время начала не может быть позже времени окончания'})

                start_date = '{} {}'.format(fdate, ftime_from)
                end_date = '{} {}'.format(fdate, ftime_to)

                city = City.objects.filter(c_name=fcity)[0]
                city_id = city.id

                if fpoint == '':
                    flights_count = self.sqlmanager.query(
                        u'''
                        SELECT COUNT(*)
                        FROM orders_flight of INNER JOIN point p ON of.point_id = p.id
                            INNER JOIN flight_order fo ON of.id = fo.flight_id
                            INNER JOIN orders_order oo ON oo.id = fo.order_id
                            INNER JOIN client c ON c.id = oo.address_id
                            INNER JOIN geo_street gs ON c.street_id = gs.id
                        WHERE of.created_at BETWEEN %s AND %s AND p.city_id = %s;''',
                        [start_date, end_date, city_id, ]
                    )
                else:
                    point = Point.objects.get(pk=int(fpoint))
                    flights_count = self.sqlmanager.query(
                        u'''
                        SELECT COUNT(*)
                        FROM orders_flight of INNER JOIN flight_order fo ON of.id = fo.flight_id
                            INNER JOIN orders_order oo ON oo.id = fo.order_id
                            INNER JOIN client c ON c.id = oo.address_id
                            INNER JOIN geo_street gs ON c.street_id = gs.id
                        WHERE of.created_at BETWEEN %s AND %s AND of.point_id = %s;''',
                        [start_date, end_date, point.id]
                    )

                return JsonResponse({'result': 'ok', 'routesCount': flights_count[0][0], })

            except Exception as e:
                print(e)
                return JsonResponse({'result': 'fail', 'errormsg': str(e)})
        else:
            return redirect('/')

    @method_decorator(csrf_protect)
    def get_routes(self, request):

        if request.is_ajax and request.POST:
            try:
                # TODO: сделать проверку на корректность введенных данных
                fcity = request.POST.get('city')
                fdate = request.POST.get('date')
                fpoint = request.POST.get('point')
                ftime_from = request.POST.get('timeFrom')
                ftime_to = request.POST.get('timeTo')

                if ftime_from > ftime_to:
                    return JsonResponse({'result': 'fail',
                                         'errormsg': 'Время начала не может быть позже времени окончания'})

                start_date = '{} {}'.format(fdate, ftime_from)
                end_date = '{} {}'.format(fdate, ftime_to)

                # TODO: должна быть проверка на существование города в базе
                city = City.objects.filter(c_name=fcity)[0]
                city_id = city.id
                center = [city.lat, city.lon]

                if fpoint == '':
                    flights = self.sqlmanager.query(
                        u'''
                        SELECT p.address, CONCAT('{}', ', ', gs.street_name, ', ', c.house_num), c.lat, c.lon
                        FROM orders_flight of INNER JOIN point p ON of.point_id = p.id
                            INNER JOIN flight_order fo ON of.id = fo.flight_id
                            INNER JOIN orders_order oo ON oo.id = fo.order_id
                            INNER JOIN client c ON c.id = oo.address_id
                            INNER JOIN geo_street gs ON c.street_id = gs.id
                        WHERE of.created_at BETWEEN %s AND %s AND p.city_id = %s;'''.format(city.c_name),
                        [start_date, end_date, city_id, ]
                    )
                else:
                    point = Point.objects.get(pk=int(fpoint))
                    flights = self.sqlmanager.query(
                        u'''
                        SELECT p.address, CONCAT('{}', ', ', gs.street_name, ', ', c.house_num), c.lat, c.lon
                        FROM orders_flight of INNER JOIN point p ON of.point_id = p.id
                            INNER JOIN flight_order fo ON of.id = fo.flight_id
                            INNER JOIN orders_order oo ON oo.id = fo.order_id
                            INNER JOIN client c ON c.id = oo.address_id
                            INNER JOIN geo_street gs ON c.street_id = gs.id
                        WHERE of.created_at BETWEEN %s AND %s AND of.point_id = %s;'''.format(city.c_name),
                        [start_date, end_date, point.id, ]
                    )

                routes = {}
                for src, dst, lat, lon in flights:
                    routes[src] = routes.get(src, []) + [(dst, [lat, lon])]

                return JsonResponse({'result': 'ok', 'city': fcity, 'center': center, 'routes': routes, })

            except Exception as e:
                return JsonResponse({'result': 'fail', 'errormsg': str(e)})
        else:
            return redirect('/')

