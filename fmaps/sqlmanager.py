
from django.db import connection


class SqlManager(object):
    def __init__(self):
        super(SqlManager, self).__init__()

    def query(self, query, params):
        cursor = connection.cursor()
        try:
            print(cursor.execute(query, params))
            return cursor.fetchall()
        except Exception as e:
            print(e)
            cursor.close()
            return False
