from django.apps import AppConfig


class FmapsConfig(AppConfig):
    name = 'fmaps'
